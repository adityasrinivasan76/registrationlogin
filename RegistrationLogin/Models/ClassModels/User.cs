﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegistrationLogin.Models
{
    [MetadataType(typeof(MetaUser))]
    public partial class User
    {
        public string ConfirmPassword { get; set; }
    }
    public class MetaUser
    {
        public long ID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        //[DataType(DataType.Date)]
        // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{dd/mm/yyyy}")]
        public System.DateTime DOB { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        public string Gender { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        // [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DataType(DataType.EmailAddress)]
        public string EmailID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        public string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        [MinLength(5, ErrorMessage = "Minimum 5 characters Required")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords not Matching")]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }

}