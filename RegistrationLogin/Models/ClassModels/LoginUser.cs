﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegistrationLogin.Models.ClassModels
{
    public class LoginUser
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        public string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        [MinLength(5, ErrorMessage = "Minimum 5 characters Required")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}