﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationLogin.Models.ClassModels;

namespace RegistrationLogin.Controllers
{
    public class AfterLoginController : Controller
    {
        // GET: AfterLogin
        [Authorize]
        public ActionResult Index()
        {
            if (Session["userName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return View();
            }

        }
    }
}