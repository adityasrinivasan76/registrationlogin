﻿using RegistrationLogin.Models;
using RegistrationLogin.Models.ClassModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace RegistrationLogin.Controllers
{
    public class HomeController : Controller
    {
        HRMSEntities db = new HRMSEntities();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        //Post :Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginUser u)
        {
            var valid = db.Users.Where(l => l.UserName == u.UserName).FirstOrDefault();
            try
            {
                if (valid != null)
                {
                    if (String.Compare(PWHashing.Hash(u.Password), valid.Password) == 0)
                    {
                        TicketAuth ticket = new TicketAuth();
                        HttpCookie cookie = ticket.Encrypt(valid.UserName.ToString());
                        HttpContext.Response.Cookies.Add(cookie);
/*
                        int timeOut = u.RememberMe ? 52600 : 1; //52600 minutes = 1 year
                        var tokenGiven = new FormsAuthenticationTicket(u.UserName, u.RememberMe, timeOut);
                        string encrypt = FormsAuthentication.Encrypt(tokenGiven);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypt);
                        cookie.Expires = DateTime.Now.AddMinutes(timeOut);
                        
                        Response.Cookies.Add(cookie);*/
                        Session["userName"] = u.UserName;
                      //  valid.Token = Convert.ToString(ticket.Decrypt());
                      //  db.SaveChanges();
                        TempData["LoginSuccessMessage"] = "<script>alert('Login Successfull')</script>";
                        /*HttpCookie myCookie = Request.Cookies["cookie"];
                        valid.Token = myCookie.Value;*/
                        return RedirectToAction("Index", "AfterLogin");
                    }
                    else
                    {
                        TempData["msg"] = "<script>alert('Username or Password is Incorrect')</script>";
                        return View();
                    }
                }
                else
                {
                    TempData["msg"] = "<script>alert('Username or Password is Incorrect')</script>";
                    return View();
                }
            }
            catch(Exception ex )
            {
                /*
                Log log = new Log();
                log.LogFile(exe.Message, exe.ToString(),exe.LineNumber());*/
                return View();
            }
                      
        }
        //GET : SignUp
        public ActionResult Signup()
        {
            return View();
        }
        //POST : Signup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signup(User u)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var isExistUserName = IsUsernameExist(u.UserName);
                    var isExistEmail = IsEmailExist(u.EmailID);
                    if (isExistUserName)
                    {
                        ModelState.AddModelError("UsernameExist", "UserName Already Exist");
                        return View(u);
                    }
                    if (isExistEmail)
                    {
                        ModelState.AddModelError("EmailIDExist", "Email ID Already Exist");
                        return View(u);
                    }

                    u.Password = PWHashing.Hash(u.Password);
                    u.ConfirmPassword = PWHashing.Hash(u.ConfirmPassword);

                    db.Users.Add(u);
                    int a = db.SaveChanges();
                    if (a > 0)
                    {
                        TempData["RegisterFail"]= "<script>alert('Registration Successfull, Redirecting you to Login page')</script>";
                        Task.WaitAll(Task.Delay(5000));
                        ModelState.Clear();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["RegisterFail"] = "<scrtipt>alert('Registration Failed')</script>";
                    }
                }
            }
            catch (Exception)
            {
                /*
                Log log = new Log();
                log.LogFile(exe.Message, exe.ToString(), exe.LineNumber());*/
                return View();
            }
           
            return View();
        }
        [Authorize]
        public ActionResult LogOut()
        {
            Session.Abandon();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            return RedirectToAction("Index", "Home");
        }

        [NonAction]
        public bool IsUsernameExist(string username)
        {
            var v = db.Users.Where(u => u.UserName == username).FirstOrDefault();
            return v == null ? false : true;
        }

        [NonAction]
        public bool IsEmailExist(string EmailID)
        {
            var v = db.Users.Where(u => u.EmailID == EmailID).FirstOrDefault();
            return v == null ? false : true;
        }

    }        
        
}