﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegistrationLogin;
using RegistrationLogin.Controllers;
using RegistrationLogin.Models.ClassModels;

namespace RegistrationLoginTest
{
    [TestClass]
    public class HomeControllerTest
    {/*
        [TestMethod]
        [HttpGet]
        public void RegisterMethodTest()
        {
            var controller = new HomeController();
            var result = controller.Signup() as ViewResult;
            Assert.IsNotNull(result);
        }*/
        [TestMethod]
        public void LoginMethodTest()
        {
            LoginUser login = new LoginUser();
            login.UserName = "asd";
            login.Password = "asdfg";
            login.Password = PWHashing.Hash(login.Password);
            login.RememberMe = true;
            var controller = new HomeController();
            var result = controller.Index(login) as ViewResult;
            Assert.IsNotNull(result);
        }
        
    }
}
