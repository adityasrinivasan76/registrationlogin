﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegistrationLogin;
using RegistrationLogin.Controllers;
using RegistrationLogin.Models.ClassModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RegistrationLoginTest
{
    [TestClass]
    class LoginTest
    {
        [TestMethod]
        public void LoginMethodTest()
        {
            LoginUser login = new LoginUser();
            login.UserName = "asd";
            login.Password = "asdfg";
            login.Password = PWHashing.Hash(login.Password);
            login.RememberMe = true;
            var controller = new HomeController();
            var result = controller.Index(login) as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
